angular.module('starter.services', [])

.factory('Clothes', function($http) {

  let corsURL = "https://cors-anywhere.herokuapp.com/";


  return {
    get: function(word) {

    let params =
`query {
  results (shops: ["hm", "bpshop"] ,name: "${word}", size: 40) {
    name
    image
    price
    type
  }
}`;

      return $http.get(`${corsURL}https://cloth-searcher.herokuapp.com/graphql?query=${params}`);
    }
  };
});
