angular.module('starter.controllers', [])

.controller('FavoritesController', function($scope) {

})

.controller('SearchController', function($scope, Clothes) {
    $scope.loading = false;

    $scope.searchChlothes = function (word) {
        $scope.loading = true;

        Clothes.get(word)
        .then(function success(response) {

            $scope.items = response.data.data.results;
            $scope.loading = false;
            console.log($scope.items);

        }, function error(response) {
            $scope.loading = false;
            console.error("FAILED");
            console.log(response);
        });
    };
})

.controller('SettingsController', function($scope) {
  $scope.settings = {
    enableShopSearch: false,

    // H&M
    enableShop1: false,

    // Zara
    enableShop2: false,

    // Bershka
    enableShop3: false,

    // BP Shop
    enableShop4: false
  };
});
