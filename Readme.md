﻿iRuházz app
=======

**Az iRuházz alkalmazás tervek szerint egy ruha kereső alkalmazás lesz, amely specifikusan tud egyszerre több boltban is keresni **(pl.: Zara, H&m, Bershka, Bp shop). **Mindezek mellett tervben van kiegészítő funkciók, mint például értesítés a kedvencek közé berakott tételek árának változásakor. Mindezt elérheti a felhasználó IOS, Android, Windows phone eszközökön, valamint egy webes felületen keresztül**